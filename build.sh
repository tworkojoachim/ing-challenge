#!/bin/bash

# Zmienne konfiguracyjne aplikacji
APP_NAME="Ing challenge"
# shellcheck disable=SC2034
MAVEN_OPTS="-Xms256m -Xmx512m"
SPRING_PROFILES_ACTIVE="dev"

# Budowanie aplikacji za pomocą Maven
echo "Rozpoczynam budowanie aplikacji ${APP_NAME}"
mvn clean package -Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}

# Sprawdzenie, czy proces budowania zakończył się sukcesem
if [ $? -ne 0 ]; then
  echo "Błąd: Budowanie aplikacji ${APP_NAME} nie powiodło się"
  exit 1
fi

echo "Budowanie aplikacji ${APP_NAME} zakończone pomyślnie"
exit 0
