#!/bin/bash

# Zmienne konfiguracyjne aplikacji
APP_NAME="Ing challenge"
JAR_FILE="./target/Ing-challenge-1.jar"
JAVA_OPTS="-Xms256m -Xmx512m"
SPRING_PROFILES_ACTIVE="dev"

# Sprawdzenie, czy plik JAR istnieje
if [ ! -f "${JAR_FILE}" ]; then
  echo "Błąd: Plik ${JAR_FILE} nie istnieje"
  exit 1
fi

# Uruchomienie aplikacji za pomocą Java
echo "Uruchamiam aplikację ${APP_NAME}"
java ${JAVA_OPTS} -jar ${JAR_FILE} --spring.profiles.active=${SPRING_PROFILES_ACTIVE}

# Sprawdzenie, czy proces uruchomienia zakończył się sukcesem
if [ $? -ne 0 ]; then
  echo "Błąd: Uruchamianie aplikacji ${APP_NAME} nie powiodło się"
  exit 1
fi

echo "Aplikacja ${APP_NAME} uruchomiona pomyślnie"
exit 0
