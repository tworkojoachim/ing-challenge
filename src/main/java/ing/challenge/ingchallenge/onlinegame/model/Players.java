package ing.challenge.ingchallenge.onlinegame.model;

import io.swagger.v3.oas.annotations.media.ArraySchema;
import jakarta.validation.constraints.Size;

import java.util.List;

public record Players(Integer groupCount,
                      @ArraySchema @Size(max = 20000) List<Clan> clans) {
}