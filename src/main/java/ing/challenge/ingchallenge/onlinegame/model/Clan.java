package ing.challenge.ingchallenge.onlinegame.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;

public record Clan(@Schema(example = "10", type = "int32") @Size(min = 1, max = 1000)
                   Integer numberOfPlayers,
                   @Schema(example = "500", type = "int32") @Size(min = 1, max = 100_000)
                   Integer points) implements Comparable<Clan> {

    @Override
    public int compareTo(Clan o) {
        int cmp = o.points.compareTo(this.points);
        if (cmp != 0) {
            return cmp;
        }
        return this.numberOfPlayers.compareTo(o.numberOfPlayers);
    }
}