package ing.challenge.ingchallenge.onlinegame;

import ing.challenge.ingchallenge.onlinegame.logic.GameApi;
import ing.challenge.ingchallenge.onlinegame.model.Clan;
import ing.challenge.ingchallenge.onlinegame.model.Players;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/onlinegame")
public class GameController {

    private final GameApi gameApi;

    @PostMapping(value = "/calculate", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Calculate order")
    List<List<Clan>> calculate(@RequestBody @NotNull @Valid Players players) {
        return gameApi.calculate(players);
    }
}
