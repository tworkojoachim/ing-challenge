package ing.challenge.ingchallenge.onlinegame.logic;

import ing.challenge.ingchallenge.onlinegame.model.Clan;
import ing.challenge.ingchallenge.onlinegame.model.Players;

import java.util.List;

public interface GameApi {

    List<List<Clan>> calculate(Players players);
}
