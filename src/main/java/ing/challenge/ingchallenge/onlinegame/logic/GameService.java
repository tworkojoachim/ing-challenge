package ing.challenge.ingchallenge.onlinegame.logic;

import ing.challenge.ingchallenge.onlinegame.model.Clan;
import ing.challenge.ingchallenge.onlinegame.model.Players;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
class GameService implements GameApi {

    @Override
    public List<List<Clan>> calculate(Players players) {
        List<List<Clan>> groups = new ArrayList<>();
        int size = players.groupCount();
        List<Clan> currentGroup = new ArrayList<>(size);
        List<Clan> sortedClans = new ArrayList<>(players.clans());
        Collections.sort(sortedClans);
        int i = 0;
        int clansSize = sortedClans.size();
        while (i < clansSize) {
            Clan current = sortedClans.get(i);
            if (current.numberOfPlayers() <= size) {
                currentGroup.add(current);
                size -= current.numberOfPlayers();
                sortedClans.remove(current);
                clansSize--;
                if (size == 0 || i == clansSize) {
                    groups.add(new ArrayList<>(currentGroup));
                    currentGroup.clear();
                    i = 0;
                    size = players.groupCount();
                }
            } else if (i + 1 == clansSize) {
                groups.add(new ArrayList<>(currentGroup));
                currentGroup.clear();
                i = 0;
                size = players.groupCount();
            } else {
                i++;
            }
        }
        return groups;
    }
}