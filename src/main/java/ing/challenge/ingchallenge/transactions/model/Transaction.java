package ing.challenge.ingchallenge.transactions.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;

public record Transaction(@Schema(example = "32309111922661937852684864") @Size(min = 26, max = 26) String debitAccount,
                          @Schema(example = "66105036543749403346524547") @Size(min = 26, max = 26) String creditAccount,
                          Float amount) {}