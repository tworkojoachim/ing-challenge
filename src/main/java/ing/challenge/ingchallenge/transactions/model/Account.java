package ing.challenge.ingchallenge.transactions.model;

import lombok.Builder;
import lombok.Getter;

@Getter
public class Account {

    String account;
    int debitCount;
    int creditCount;
    float balance;

    public Account(String account) {
        this.account = account;
        this.creditCount = 0;
        this.debitCount = 0;
        this.balance = 0;
    }

    public void decreaseBalance(float amount) {
        this.debitCount ++;
        this.balance -= amount;
        this.balance = round();
    }

    public void increaseBalance(float amount) {
        this.creditCount ++;
        this.balance += amount;
        this.balance = round();
    }

    private float round() {
        return Math.round(this.balance * 100.0F) / 100.0F;
    }
}