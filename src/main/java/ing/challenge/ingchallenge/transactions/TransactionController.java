package ing.challenge.ingchallenge.transactions;

import ing.challenge.ingchallenge.transactions.logic.TransactionReportApi;
import ing.challenge.ingchallenge.transactions.model.Account;
import ing.challenge.ingchallenge.transactions.model.Transaction;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/transactions")
class TransactionController {

    private final TransactionReportApi transactionReportApi;

    @PostMapping(value = "/report", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Execute report", operationId = "report")
    @ApiResponse(responseCode = "200", description = "Successful operation", content = {
            @Content(array = @ArraySchema(schema = @Schema(implementation = Account.class)))
    })
    List<Account> executeTransactionsReport(@ArraySchema(maxItems = 100000, schema = @Schema(implementation = Transaction.class))
                                            @RequestBody @NotNull @Valid List<Transaction> transactions) {
        return transactionReportApi.executeTransactionsReport(transactions);
    }
}