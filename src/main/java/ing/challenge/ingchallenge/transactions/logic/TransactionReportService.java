package ing.challenge.ingchallenge.transactions.logic;

import ing.challenge.ingchallenge.transactions.model.Account;
import ing.challenge.ingchallenge.transactions.model.Transaction;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
class TransactionReportService implements TransactionReportApi {

    @Override
    public List<Account> executeTransactionsReport(List<Transaction> transactions) {
        Map<String, Account> accounts = new ConcurrentHashMap<>();

        transactions.parallelStream().forEach(transaction -> {
            Account debitAccount = accounts.computeIfAbsent(transaction.debitAccount(), Account::new);
            Account creditAccount = accounts.computeIfAbsent(transaction.creditAccount(), Account::new);

            debitAccount.decreaseBalance(transaction.amount());
            creditAccount.increaseBalance(transaction.amount());
        });

        List<Account> accountList = new ArrayList<>(accounts.values());
        accountList.sort(Comparator.comparing(Account::getAccount));
        return accountList;
    }
}