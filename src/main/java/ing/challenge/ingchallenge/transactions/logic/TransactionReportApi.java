package ing.challenge.ingchallenge.transactions.logic;

import ing.challenge.ingchallenge.transactions.model.Account;
import ing.challenge.ingchallenge.transactions.model.Transaction;

import java.util.List;

public interface TransactionReportApi {

    List<Account> executeTransactionsReport(List<Transaction> transactions);
}