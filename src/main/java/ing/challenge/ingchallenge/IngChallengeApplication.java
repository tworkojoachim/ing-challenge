package ing.challenge.ingchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(IngChallengeApplication.class, args);
	}

}