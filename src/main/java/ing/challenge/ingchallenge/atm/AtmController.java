package ing.challenge.ingchallenge.atm;

import ing.challenge.ingchallenge.atm.logic.ATMOrderApi;
import ing.challenge.ingchallenge.atm.model.ATM;
import ing.challenge.ingchallenge.atm.model.Task;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/atms")
class AtmController {

    private final ATMOrderApi atmOrderApi;

    @PostMapping(value = "/calculateOrder", consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(description = "Calculates ATMs order for service team", operationId = "calculate")
    List<ATM> calculateOrder(@ArraySchema(schema = @Schema(implementation = Task.class))
                                            @RequestBody @NotNull @Valid List<Task> serviceTasks) {
        return atmOrderApi.calculateOrder(serviceTasks);
    }
}
