package ing.challenge.ingchallenge.atm.logic;

import ing.challenge.ingchallenge.atm.model.ATM;
import ing.challenge.ingchallenge.atm.model.Task;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
class ATMOrderService implements ATMOrderApi {

    @Override
    public List<ATM> calculateOrder(List<Task> serviceTasks) {
        return serviceTasks.parallelStream()
                .sorted(Comparator.comparing(Task::region)
                        .thenComparing(Task::getPriority))
                .collect(Collectors.toMap(task -> Arrays.asList(task.region(), task.atmId()),
                        ATM::of, (atm1, atm2) -> atm1, LinkedHashMap::new))
                .values().parallelStream()
                .toList();
    }
}
