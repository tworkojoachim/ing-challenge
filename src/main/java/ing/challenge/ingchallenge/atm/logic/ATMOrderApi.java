package ing.challenge.ingchallenge.atm.logic;

import ing.challenge.ingchallenge.atm.model.ATM;
import ing.challenge.ingchallenge.atm.model.Task;

import java.util.List;

public interface ATMOrderApi {

    List<ATM> calculateOrder(List<Task> serviceTasks);
}