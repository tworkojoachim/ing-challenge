package ing.challenge.ingchallenge.atm.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum RequestType {

    STANDARD(4),
    SIGNAL_LOW(3),
    PRIORITY(2),
    FAILURE_RESTART(1);

    @Getter
    private final Integer priority;
}