package ing.challenge.ingchallenge.atm.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;

public record Task(@Schema(example = "STANDARD", description = "Type of request") RequestType requestType,
                   @Schema(example = "10", type = "int32") @Size(min = 1, max = 9999) Integer region,
                   @Schema(example = "500", type = "int32") @Size(min = 1, max = 9999) Integer atmId) implements Comparable<Task> {

    public Integer getPriority() {
        return this.requestType.getPriority();
    }

    @Override
    public int compareTo(Task o) {
        int regionCompare = this.region.compareTo(o.region);
        if (regionCompare != 0) {
            return regionCompare;
        }
        return this.atmId.compareTo(o.atmId);
    }
}