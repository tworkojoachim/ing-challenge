package ing.challenge.ingchallenge.atm.model;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.Size;

public record ATM(@Schema(example = "10", type = "int32") @Size(min = 1, max = 9999) Integer region,
                  @Schema(example = "500", type = "int32") @Size(min = 1, max = 9999) Integer atmId) {

    public static ATM of(Task task) {
        return new ATM(task.region(), task.atmId());
    }
}